import { ACTION_TYPES } from '../constants/actionTypes';
import {Auth, User} from '../../model';

interface AppState {
    isLoading: boolean,
    users: User[],
    auth?: Auth
}

const initialState: AppState = {
    isLoading: false,
    auth:undefined,
    users:[],
};

export default (state = initialState, action: any) => {

  switch (action.type) {
    case ACTION_TYPES.SPLASH_LAUNCHED:
      return {
        ...state,
      };
      case ACTION_TYPES.AUTH:
          return {
              ...state,
              auth: action.auth
          };
      case ACTION_TYPES.UPDATE_USER_IMAGE:

          if (!state.auth) {
              return state;
          }

          return {
              ...state,
              auth: { ...state.auth, user: {...state.auth.user, imageUrl: action.url } }
          };
      case ACTION_TYPES.ADD_USER:
          return {
              ...state,
              users: [...state.users, action.user ]
          };
      case ACTION_TYPES.REMOVE_USER:
          return {
              ...state,
              users: state.users.filter((u:User) => u.id !== action.user.id)
          };
      case ACTION_TYPES.SET_USERS:
          return {
              ...state,
              users: action.users
          };
      case ACTION_TYPES.CREATE_SERVICE:

          let services = getAssets(state,'services');

          if (services) {
              return setAssets(state,'services', [...services,action.service])
          }

          return state;

      case ACTION_TYPES.CREATE_ORDER:

          let ordered = getAssets(state,'ordered');

          if (ordered) {
              return setAssets(state,'ordered',[...ordered,action.order])
          }

          return state;

      case ACTION_TYPES.RECEIVE_ORDER:

          orders = getAssets(state,'orders');

          if (orders) {
              return setAssets(state,'orders',[...orders,action.order])
          }

          return state;

    case ACTION_TYPES.RECEIVE_OFFER:

          orders = getAssets(state,'offers');

          if (orders) {
              return setAssets(state,'offers',[...orders,action.order])
          }

          return state;

    case ACTION_TYPES.REMOVE_OFFER:

          orders = getAssets(state,'offers');

          if (orders) {
              return setAssets(state,'offers',[...orders.filter(o => o.id !== action.order.id)])
          }

          return state;

      case ACTION_TYPES.UPDATE_ORDERS:

          orders = getAssets(state,'orders');

          if (orders) {
              return setAssets(state,'orders',[
                  ...orders.filter(o => !action.orders.find(oo => o.id === oo.id)),
                  ...action.orders
              ])
          }

          return state;

        case ACTION_TYPES.REMOVE_ORDERS:

          orders = getAssets(state,'orders');

          if (orders) {
              return setAssets(state,'orders',[
                  ...orders.filter(o => !action.orders.find(oo => o.id === oo.id)),
              ])
          }

          return state;


      case ACTION_TYPES.UPDATE_ORDERED:

          orders = getAssets(state,'ordered');

          if (orders) {
              return setAssets(state,'ordered',[
                  ...orders.filter(o => !action.orders.find(oo => o.id === oo.id)),
                  ...action.orders
              ])
          }

          return state;
    default:
      return state;
  }

};

// hoisting
let orders;

const getAssets = (state, field: string) : any[] => {

    let assets = state.auth && state.auth.indexData && state.auth.indexData.assets;

    return assets ? [...assets[field]] : [];
}

const setAssets = (state, field?: string, value?: any[]) => {

    return {...state, auth: {...state.auth, indexData:{...state.auth.indexData, assets: {...state.auth.indexData.assets, [field]:value}}}}
}
