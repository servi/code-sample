import { ACTION_TYPES } from '../constants/actionTypes';
import {Message } from '../../model';

interface ChatState {
    messages: Message[]
}

const initialState: ChatState = {
    messages:[]
};

export default (state = initialState, action: any) => {

  switch (action.type) {

      case ACTION_TYPES.UPDATE_MESSAGE:

          return {...state, messages: [...state.messages.map(m => m.id === action.message.id ? action.message : m)]};

      case ACTION_TYPES.NEW_MESSAGE:

          return {...state, messages: [...state.messages, action.message]};

      case ACTION_TYPES.SET_MESSAGES:

          return {...state, messages: action.messages};
    default:
      return state;
  }

};
