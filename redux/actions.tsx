
import store from './store'
import actionTypes from './constants/actionTypes'
import {Message} from '../model';

export const auth = (identity:any) => {
    store.dispatch({
        type: actionTypes.AUTH,
        auth: identity
    });
}

export const updateUserImage = (url:string) => {
    store.dispatch({
        type: actionTypes.UPDATE_USER_IMAGE,
        url
    });
}

export const logout = () => {
    store.dispatch({
        type: actionTypes.AUTH,
        auth: null
    });
}

export const createService = (service) => {
    store.dispatch({
        type: actionTypes.CREATE_SERVICE,
        service
    });
}

export const createOrder = (order) => {
    store.dispatch({
        type: actionTypes.CREATE_ORDER,
        order
    });
}
export const receiveOrder = (order) => {
    store.dispatch({
        type: actionTypes.RECEIVE_ORDER,
        order
    });
}
export const receiveOffer = (order) => {
    store.dispatch({
        type: actionTypes.RECEIVE_OFFER,
        order
    });
}
export const removeOffer = (order) => {
    store.dispatch({
        type: actionTypes.REMOVE_OFFER,
        order
    });
}
export const updateOrders = (orders) => {
    store.dispatch({
        type: actionTypes.UPDATE_ORDERS,
        orders
    });
}
export const removeOrders = (orders) => {
    store.dispatch({
        type: actionTypes.REMOVE_ORDERS,
        orders
    });
}
export const updateOrdered = (orders) => {
    store.dispatch({
        type: actionTypes.UPDATE_ORDERED,
        orders
    });
}


export const setUsers = (users) => {
    store.dispatch({
        type: actionTypes.SET_USERS,
        users
    });
}

export const addUser = (user) => {
    store.dispatch({
        type: actionTypes.ADD_USER,
        user
    });
}

export const removeUser = (user) => {
    store.dispatch({
        type: actionTypes.REMOVE_USER,
        user
    });
}

export const newMessage = (message:Message) => {
    store.dispatch({
        type: actionTypes.NEW_MESSAGE,
        message
    });
}
export const updateMessage = (message) => {
    store.dispatch({
        type: actionTypes.UPDATE_MESSAGE,
        message
    });
}

export const setMessages = (messages) => {
    store.dispatch({
        type: actionTypes.SET_MESSAGES,
        messages
    });
}


export default {
    auth,
    logout,
    updateOrders,
    removeOrders,
    updateOrdered,
    setUsers,
    removeUser,
    addUser,
    createService,
    createOrder,
    removeOffer,
    receiveOrder,
    receiveOffer,
    updateUserImage,
    newMessage,
    updateMessage,
    setMessages
}