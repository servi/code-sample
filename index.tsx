import { connect } from 'react-redux';

import Component from './Component';

const mapStateToProps = (state:any) => ({
    auth: state.app.auth,
    user: state.app.auth ? state.app.auth.user : undefined,
    categories: state.app.auth ? state.app.auth.indexData.dicts.categoriesFlat: [],
    users: state.app.users,
    offers: state.app.auth ? state.app.auth.indexData.assets.offers: [],
    orders: state.app.auth ? state.app.auth.indexData.assets.orders: [],
    ordered: state.app.auth ? state.app.auth.indexData.assets.ordered : [],
});

const mapDispatchToProps = () => ({});

const container = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default container;
