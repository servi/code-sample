import * as React from 'react';
import {ImageBackground, View} from 'react-native';

import {Text} from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale';
import {TYPOGRAPHY} from '../../styles/typography';
import IMAGES from '../../../constants/images';
import {Category} from '../../../model';
import Toast from '../../../lib/Toast'; // https://github.com/kohver/react-native-touchable-scale
import Icon from '../../../lib/layout/Icon'; // https://github.com/kohver/react-native-touchable-scale

export interface Props {
    category: Category,
    onPress: () => void,
}

interface State {

}

class NiceListItem extends React.PureComponent<Props, State> {

    getCategory()
    {
        return this.props.category;
    }

    countServices(category) {
        return category.service_not_mine_count + category.children.map(c => this.countServices(c)).reduce((a,b) => a + b, 0);
    }

  render() {

      let u = this.getCategory();

      // @ts-ignore
      return  <TouchableScale
          key={u.id}
          friction={90} //
          tension={100} // These props are passed to the parent component (here TouchableScale)
          style={{ marginBottom:4, marginHorizontal:4 }}
          activeScale={0.85} //
          onPress={() => this.props.onPress()}
      >
          <ImageBackground
              style={{flex:1,borderRadius: 10, borderColor:'white', borderWidth: 5}}
              source={ u.imageUrl ? {
                  uri: u.imageUrl,
              } : IMAGES.BACKGROUND_IMAGE }
          >

          <View style={{flex:1, width:'100%', paddingLeft:5}}>

              <Text style={{
                  color: 'white',
                  textShadowColor:TYPOGRAPHY.COLOR.SecondaryDarker,
                  textShadowRadius:15,
                  textShadowOffset:{width:0,height:0},
                  fontSize:33,
                  alignSelf:'center'
              }}>{u.name}</Text>

              <Text style={{
                  padding:20,
                  color: 'white',
                  textShadowColor:TYPOGRAPHY.COLOR.SecondaryDarker,
                  textShadowRadius:15,
                  textShadowOffset:{width:0,height:0},
                  alignSelf:'center'
              }}>{u.description || '... rozumie sie samo przez się'}</Text>

          </View>

          <View style={{
              backgroundColor:'white',
              padding:0, borderRadius:20,
              position:'absolute',
              bottom:-5, right:-5
          }}>
              <Icon
                  color={TYPOGRAPHY.COLOR.SecondaryDarker}
                  text={u.children && u.children.length ? u.children.length+'' : ''}
                  size={14}
                  textSize={14}
                  textMargin={0}
                  padding={4}
                  onPress={() => u.children && Toast.good(u.children.map(c => c.name + (c.children && c.children.length ? ' ('+c.children.length+')' : '')).join(', '))}
                  name={u.children && u.children.length ? "angle-double-right" : "check-circle"}
              />
          </View>

          <View style={{
              backgroundColor:'white',
              padding:0, borderRadius:20,
              position:'absolute',
              top:-5, right:-5
          }}>
              <Icon
                  color={TYPOGRAPHY.COLOR.Success}
                  text={this.countServices(u)}
                  size={14}
                  textSize={14}
                  textMargin={0}
                  padding={4}
                  name={"briefcase"}
              />
          </View>

          </ImageBackground>

      </TouchableScale>
  }
}

export default NiceListItem;
