import * as React from 'react';
import {Animated, Dimensions, PanResponder, View} from 'react-native';
import {Overlay, Text} from 'react-native-elements'
import {BACKGROUND_IMAGE, FACE_3} from '../../../constants/images'
import Icon from '../../../lib/layout/Icon';
import {Category, Service, User} from '../../../model';
import {TYPOGRAPHY} from '../../styles/typography';
import DeviceService from '../../../service/DeviceService';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

import Image from 'react-native-fast-image'

export interface Props {
    services: Service[]
    visible: boolean
    category: Category
    users: User[]
    onClose:(slashed:Service[]) => void
    // onSlash:(services:Service[]) => void
}

interface State {
    currentIndex: number,
    slashed:Service[]
}

class TinderWidget extends React.PureComponent<Props, State> {

    PanResponder;
    position;
    rotate;
    yesOpacity;
    noOpacity;
    nextCardOpacity;
    nextCardScale;
    rotateAndTranslate;

    constructor(props)
    {
        super(props);

        DeviceService.init();

        this.position = new Animated.ValueXY();

        this.state = {
            currentIndex: 0,
            slashed:[],
        }

        this.rotate = this.position.x.interpolate({
            inputRange:[-SCREEN_WIDTH/2,0,SCREEN_WIDTH/2],
            outputRange:['-10deg','0deg','10deg'],
            extrapolate:'clamp'
        });

        this.rotateAndTranslate = {
            transform:[
                {
                    rotate: this.rotate
                },
                ...this.position.getTranslateTransform()
            ],
        };

        this.yesOpacity = this.position.x.interpolate({
            inputRange:[-SCREEN_WIDTH/2,0,SCREEN_WIDTH/2],
            outputRange:[0,0,1],
            extrapolate:'clamp'
        });
        this.noOpacity= this.position.x.interpolate({
            inputRange:[-SCREEN_WIDTH/2,0,SCREEN_WIDTH/2],
            outputRange:[1,0,0],
            extrapolate:'clamp'
        });

        this.nextCardOpacity = this.position.x.interpolate({
            inputRange:[-SCREEN_WIDTH/2,0,SCREEN_WIDTH/2],
            outputRange:[1,0,1],
            extrapolate:'clamp'
        })
        this.nextCardScale = this.position.x.interpolate({
            inputRange:[-SCREEN_WIDTH/2,0,SCREEN_WIDTH/2],
            outputRange:[1,0.8,1],
            extrapolate:'clamp'
        })

        this.PanResponder = PanResponder.create({

            onStartShouldSetPanResponder: (evt, gestureState) => {
                return true;
            },
            onPanResponderMove: (evt, gestureState) => {

                this.position.setValue({x:gestureState.dx, y:gestureState.dy});

                return true;
            },
            onPanResponderRelease: (evt, gestureState) => {

                // DeviceService.getStorage().then(storage => console.log('stroag checked'));
                // DeviceService.getMemory().then(mem => console.log('mem checked'));

                if (gestureState.dx > 120) {

                    Animated.spring(this.position,{
                        toValue: { x: SCREEN_WIDTH+100, y: gestureState.dy }
                    }).start(() => {

                        this.setState({
                            currentIndex: this.state.currentIndex+1
                        }, () => {
                            this.position.setValue({x:0, y:0});

                            if (this.state.currentIndex === this.props.services.length) {
                                this.close();
                            }
                        })

                    });
                } else if (gestureState.dx < -120) {

                    Animated.spring(this.position,{
                        toValue:{x:-SCREEN_WIDTH-100, y: gestureState.dy}
                    }).start(() => {

                        let service = this.props.services[this.state.currentIndex];

                        this.setState({
                            currentIndex: this.state.currentIndex+1,
                            slashed:[...this.state.slashed, service]
                        }, () => {
                            this.position.setValue({x:0, y:0});

                            // this.props.onSlash(this.props.services[this.state.currentIndex-1])

                            if (this.state.currentIndex === this.props.services.length) {
                                // this.props.onSlash(this.state.slashed);
                                this.close();
                            }
                        })

                    });
                } else {

                    Animated.spring(this.position, {
                        toValue: {x: 0, y: 0},
                        friction: 4
                    }).start()
                }

                return true;
            }
        });
    }

    componentDidMount()
    {

    }

    // componentWillUpdate()
    // {
    //
    // }
    //
    // componentWillUnmount()
    // {
    //
    // }

    getCategory()
    {
        return this.props.category;
    }

    renderCard(service:Service)
    {
        let user = this.props.users.find(u => u.id === service.user_id);
        let category = this.getCategory();

        return <View style={{backgroundColor:'white', position:'absolute', bottom:0,
            justifyContent:'space-between',
            margin: 20, width: SCREEN_WIDTH -40 , padding:10, paddingLeft:0, borderRadius:10, flexDirection:'row' }}>

            <View style={{maxWidth:120, marginHorizontal:10, alignItems:'center' }}>

                <Image source={ user && user.imageUrl ? {uri: user.imageUrl} : FACE_3 }
                       style={ {
                           height:80,
                           width:80,
                           borderRadius:40,
                           borderColor:user ? TYPOGRAPHY.COLOR.Success : 'lightgray',
                           borderWidth:4,
                           marginBottom:5,
                       }}
                />

                <View style={{ marginBottom:5, alignItems:'center' }}>
                    <Text style={{color:'gray', fontSize:10,}}>{user ? [user.name].join(' ') : '@'+service.user_id }</Text>
                    { !!user && !!user.email && <Text style={{color:'gray', fontSize:10,}}>{ user.email }</Text> }
                    { !!user && !!user.phone_number && <Text style={{color:'gray', fontSize:10}}>{user.phone_number}</Text> }
                </View>

                <Text
                    style={ {
                        alignSelf:'center',
                        backgroundColor:TYPOGRAPHY.COLOR.SecondaryDarker,
                        borderRadius:5,
                        padding:4,
                        fontSize:20,
                        color:'gold',
                    }}
                >{service.price} PLN</Text>

            </View>

            <View style={{flex:1}}>
                <Text style={{color:TYPOGRAPHY.COLOR.SecondaryDarker, fontSize:20 }}>{category.name}</Text>
                <Text style={{color:'darkgrey', fontSize:14 }}>{service.description || ' #' + service.id+''}</Text>
                <Text style={{color:'darkgray', fontSize:14 }}>{category.description}</Text>
            </View>

        </View>
    }


   

    renderImage(service: Service, index)
    {
        let uri = service.image || this.props.category.imageUrl;

        if (index < this.state.currentIndex) {
            return null;// TODO zmienic jakos ...
        } else if (index === this.state.currentIndex) {
            return <Animated.View
                {...this.PanResponder.panHandlers}
                key={service.id} style={[this.rotateAndTranslate,{
                height: SCREEN_HEIGHT-70,
                width:SCREEN_WIDTH,
                margin:0,
                padding:10,
                position:'absolute',
                justifyContent:'center',
            }]}>

                <Animated.View style={{
                    opacity: this.yesOpacity,
                    transform: [{rotate:'-30deg'}],
                    position: 'absolute',
                    top: 50,
                    left: 60,
                    zIndex: 1000,
                    borderColor:'green',
                    borderRadius:10,
                    borderWidth:5,

                }}>
                    <Text style={{
                        fontSize:40,
                        padding:10,
                        color:'green',
                        textShadowColor:'white',
                        textShadowRadius:15,
                        textShadowOffset:{width:0,height:0}
                    }}>Tak</Text>
                </Animated.View>

                <Animated.View style={{
                    opacity: this.noOpacity,
                    transform: [{rotate:'30deg'}],
                    position: 'absolute',
                    top: 50,
                    right: 60,
                    zIndex: 1000,
                    borderColor:'red',
                    borderRadius:10,
                    borderWidth:5,
                }}>
                    <Text style={{
                        fontSize:40,
                        padding:10,
                        color:'red',
                        textShadowColor:'white',
                        textShadowRadius:15,
                        textShadowOffset:{width:0,height:0}
                    }}>Nie</Text>
                </Animated.View>


                <Image source={ uri ? {uri} : BACKGROUND_IMAGE }
                       style={ {
                           flex:1,
                           // width:null, height:null,
                           // resizeMode:'cover',
                           borderRadius:20,
                           borderColor:'white',
                           borderWidth:4,
                       }}
                />

                { this.renderCard(service) }

            </Animated.View>
        } else if (index === this.state.currentIndex+1) {

            return <Animated.View
                {...this.PanResponder.panHandlers}
                key={service.id} style={{
                    opacity:this.nextCardOpacity,
                    transform:[{scale:this.nextCardScale}],
                height: SCREEN_HEIGHT-70,
                width:SCREEN_WIDTH,
                margin:0,
                padding:10,
                position:'absolute',
                justifyContent:'center',
            }}>
                <Image source={ uri ? {uri} : BACKGROUND_IMAGE }
                       style={ {
                           flex:1,
                           // opacity:0.5,
                           // width:null, height:null,
                           // resizeMode:'cover',
                           borderRadius:20,
                           borderColor:'white',
                           borderWidth:4,
                       }}
                />

                { this.renderCard(service) }

            </Animated.View>
        } else {
            return null;
        }


    }

  
    close()
    {
        this.props.onClose(this.state.slashed);
    }

    renderAnimatedImages()
    {
        return this.props.services
            .map((service,i) => this.renderImage(service, i))
            .reverse();
    }

    render() {
        return <Overlay
            fullScreen={true}
            overlayStyle={{padding:0}}
            overlayBackgroundColor={'transparent'}

            containerStyle={{maxHeight: '100%', margin: 0}}
            isVisible={this.props.visible}
            onBackdropPress={() => this.close()}
        >

            <View style={{flex: 1, margin:0}}>

                <View style={{flex: 1}}>
                        { this.renderAnimatedImages() }
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingLeft:20}}>

                    <View style={{backgroundColor:'darkgray', borderRadius:30, borderColor:'white', borderWidth:5,marginBottom:3}}>
                        <Icon
                            color={TYPOGRAPHY.COLOR.SecondaryDarker}
                            size={24}
                            text="Wróć"
                            name="hiking"
                            onPress={() => this.close()}
                        />
                    </View>

                </View>

            </View>

        </Overlay>
    }
}
export default TinderWidget;
