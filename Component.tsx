import * as React from 'react';
import {FlatList, View} from 'react-native';
import { Text} from 'react-native-elements'
import uuid from 'uuid';
import Layout from '../../../lib/layout/Layout';
import Icon from '../../../lib/layout/Icon';
import SearchBar from '../../../lib/layout/SearchBar';
import Toast from '../../../lib/Toast';
import actions from '../../../redux/actions';
import api from '../../../service/ApiService';
import {Category, Order, Service, User} from '../../../model';
import SearchComponent from './SearchComponent';
import OrderServiceWidget from './OrderServiceWidget';
import TinderWidget from './TinderWidget';
import ListItem from './ListItem';
import CategoryListItem from './CategoryListItem';

export interface Props {
  name: string;
  auth:any;
  users:User[];
  user:User;
  categories: Category[],
  orders:Order[];
  offers:Order[];
  ordered:Order[];
}

interface State {
    service?: Service;
    popup:boolean,
    search: string,
    selectedCategory?:Category,
    selectedCategories?:Category[],
    services: Service[],
    showOrder:boolean,
    showTinder:boolean,
    showTinderPreview:boolean,
    includeOffline:boolean
}

class Services extends React.PureComponent<Props, State> {

    wizard = null;

    state = {
        includeOffline: false,
        service:undefined,
        popup:false,
        showOrder:false,
        showTinder:false,
        showTinderPreview:false,
        search:'',
        services: [],
        selectedCategory:undefined,
        selectedCategories:undefined,
    }

    updateSearch(search) {
        this.setState({ search });
    };

    componentDidMount()
    {
        console.disableYellowBox = true;

    }

    showOrderWidget(service:Service, user:User = undefined)
    {
        if (user) {
            service.user = user;
        }

        setTimeout(() => this.setState({showOrder:true, service}),500);
    }

    showPreviewWidget(service:Service, user:User = undefined)
    {
        if (user) {
            service.user = user;
        }

        setTimeout(() => this.setState({showTinderPreview:true, service}),500);
    }

    searchCategory()
    {
        if (this.state.selectedCategory) {

            this.setState({selectedCategory:undefined, services:[]});
        } else {

            this.setState({popup:true});
        }
    }

    getOnlineServicesList()
    {
        return this.getServicesList().filter(s => this.getUsers().find(u => u.id === s.user_id))
    }
    getServicesList()
    {
        let search =  (this.state.search||'').toLowerCase();
        let services = this.state.services;

        if (!this.state.includeOffline) {
            services = services.filter((s:Service) => !!this.getUser(s.user_id))
        }

        if (search) {
             services = services.filter((s:Service) => s.description && s.description.toLowerCase().indexOf(search) !== -1)
        }

        services = services.sort((a:Service, b:Service) => {

            if (!this.state.includeOffline) {
                return a.price -b.price;
            }

            let ua = this.getUser(a.user_id);
            let ub = this.getUser(b.user_id);

            if ((!ua && !ub) || (ua && ub)) {
                return a.price -b.price;
            } else if (ua) {
                return -1;
            } else if (ub) {
                return 1;
            }
            // TODO niemoze być inaczej.... (niedojdzie tu)

            return 0;
        });

        return services;
    }

    getCategoriesSelection() :Category[]
    {
        return this.state.selectedCategories ? this.filterCats(this.state.selectedCategories) : this.getCategories();
    }

    filterCats(cats:Category[]) {
        return cats.filter(c => c.service_not_mine_count || this.filterCats(c.children).length).map(c => ({...c,children:this.filterCats(c.children)}));
    }

    getCategories() : Category[]
    {
        return this.props.auth && this.props.auth.indexData ? this.filterCats(this.props.auth.indexData.dicts.categories) : [];
    }

    getUsers()
    {
        return this.props.users ||  [];
    }

    getUser(id)
    {
        return this.getUsers().find(u => u.id === id);
    }

    getCategoriesFlat()
    {
        return this.props.auth && this.props.auth.indexData ? this.props.auth.indexData.dicts.categoriesFlat : [];
    }

    onCategorySelect(category)
    {
        // @ts-ignore
        this.setState({services:[], service:undefined,selectedCategory: this.state.selectedCategory && this.state.selectedCategory.id === category.id ? undefined : category });
    }

    renderCategory(u:Category)
    {
        return <CategoryListItem
            key={u.id}
            category={{...u, children: this.filterCats(u.children)}}
            onPress={() => {
                if (!u.children || (u.children && !u.children.length)) {

                    this.setState({
                        selectedCategories:undefined,
                        selectedCategory: u
                    }, () => this.doSearch())

                } else {

                    this.setState({
                        selectedCategories:u.children,
                        selectedCategory: undefined
                    })
                }
            }}
        />
    }

    renderService(service:Service)
    {
        let user = this.getUsers().find((uu:User) => uu.id === service.user_id);

        let category = this.getCategoriesFlat().find(c => c.id === service.category_id) || {name:service.id + ' : ' + service.category_id};

        return <ListItem
            key={service.id}
            service={service}
            user={user}
            category={category}
            onPress={() => {
                if (user) {
                    this.showOrderWidget(service, user)
                } else {
                    this.showPreviewWidget(service, user);
                }
            }}
        />
    }

    doSearch() {

        Toast.loading();

        api.serviceSearch({
            category_id: this.state.selectedCategory!.id,

        }).then(response => {

            Toast.loaded();

            this.setState({services:response.data.data });

        }).catch (e => {

            Toast.loaded();
            console.log('SEARCH SERVIE ERROR', e);
            this.setState({popup: true});
        })
    }

    renderCategorySearch()
    {
        return <SearchComponent
            visible={this.state.popup}
            onClose={() => this.setState({ popup:false })}
            categories={this.getCategories()}
            selectedCategory={this.state.selectedCategory}
            onSelect={c => this.onCategorySelect(c)}
            onSearch={() => {
                this.setState({popup: false});

                this.doSearch();
            }}
        />
    }

    renderTinderWidget(list)
    {
        if (this.state.showTinder && this.state.selectedCategory && list.length) {
            return <TinderWidget
                users={this.props.users}
                visible={true}
                services={list}
                category={
                    this.state.selectedCategory ||
                    {name:'',description:'???',imageUrl:null, id: null, serviceType: null}
                }
                onClose={(slashed:Service[]) => {
                    this.setState({
                        showTinder: false,
                        services: this.state.services.filter((s:Service) =>
                            !slashed.find(sl => sl.id === s.id)
                        )
                    })
                }}
            />
        }

        return null;
    }

    renderTinderPreviewWidget()
    {
        if (this.state.showTinderPreview && this.state.service && this.state.selectedCategory) {
            return <TinderWidget
                users={this.props.users}
                visible={true}
                services={[this.state.service]}
                category={
                    this.state.selectedCategory ||
                    {name:'',description:'???',imageUrl:null, id: null, serviceType: null}
                }
                onClose={(slashed:Service[]) => {
                    this.setState({
                        service:undefined,
                        showTinderPreview: false,
                        services: this.state.services.filter((s:Service) =>
                            !slashed.find(sl => sl.id === s.id)
                        )
                    })
                }}
            />
        }

        return null;
    }

    renderOrderServiceWidget()
    {
        return <OrderServiceWidget
            services={this.getOnlineServicesList()}
            visible={this.state.showOrder}
            category={this.state.selectedCategory}
            service={this.state.service}
            onClose={() => this.setState({ showOrder:false })}
            onCreate={(data, isNegotiation) => {

                this.setState({showOrder: false});

                Toast.loading();

                if (isNegotiation) {

                    data.id = uuid();

                    let promises = this.getOnlineServicesList().map(s => {
                        data.service_id = s.id;

                        return  api.orderNegotiate(data);
                    });

                    Promise.all(promises).then(responses => {

                        Toast.loaded();
                        Toast.good(`Wysłane ${responses.length} zapytań. Zaczekaj na odpowiedź by utworzyć 1 lub wiecej zamówień.`);

                    }).catch(e => Toast.error(e));

                } else {

                    api.orderCreate({
                        ...data,
                    }).then(response => {

                        Toast.loaded();
                        Toast.good("Zamówienie wysłane. Zaczekaj na zatwierdzenie.");

                        actions.createOrder(response.data.data);

                    }).catch(e => {

                        Toast.loaded();

                        Toast.bad(e);

                        console.log('ORDER CREATE ERRROR', e);
                        this.setState({showOrder: true});
                    })

                }

            }}
        />
    }

    renderHeader()
    {

        let list = this.getServicesList();

        return  <View style={{flex:1, alignItems:'flex-end'}}>

            <Text style={{alignSelf:'center', color:'white',fontSize:16}}>
                {
                    // @ts-ignore
                    this.state.selectedCategory ? this.state.selectedCategory.name : null
                }
            </Text>

            <View style={{flexDirection:'row'}}>

                { !!this.state.selectedCategory && !!list.length && <Icon
                    onPress={() => this.setState({showTinder: true, service:undefined}) }
                    name="user-slash"
                /> }

                { !!this.state.selectedCategory && !!list.length && !!this.getOnlineServicesList().length && <Icon
                    onPress={() => this.setState({showOrder: true, service:undefined}) }
                    name="comments-dollar"
                /> }

                { !!this.state.selectedCategory && !!this.state.services.length && <Icon
                    onPress={() => this.setState({includeOffline:!this.state.includeOffline})}
                    name={this.state.includeOffline ? 'user-tie' : 'user-secret'}
                /> }

                { !!this.state.selectedCategories && <Icon
                    onPress={() => this.setState({selectedCategories:undefined})}
                    name="undo"
                /> }

                <Icon
                    onPress={() => this.searchCategory()}
                    name="project-diagram"
                />

                { !!this.state.selectedCategory && !!list.length && <SearchBar
                    update={search => this.updateSearch(search)}
                    value={this.state.search}
                /> }

            </View>

        </View>
    }

    render() {

        return (
            <Layout
                offers={this.props.offers}
                categories={this.props.categories}
                users={this.props.users}
                orders={this.props.orders}
                ordered={this.props.ordered}
                header={this.renderHeader()}
                user={this.props.user}>

                <View style={{paddingTop:200}}>{null}</View>

                { !!this.state.selectedCategory && <FlatList
                    keyExtractor={({}, i) => i.toString()}
                    data={this.getServicesList()}
                    renderItem={({item}) => this.renderService(item)}
                /> }

                { !this.state.selectedCategory && <FlatList
                    keyExtractor={({}, i) => i.toString()}
                    data={this.getCategoriesSelection()}
                    renderItem={({item}) => this.renderCategory(item)}
                /> }

                { this.renderCategorySearch() }
                { this.renderOrderServiceWidget() }
                { this.renderTinderWidget(this.getServicesList()) }
                { this.renderTinderPreviewWidget() }

            </Layout>
        );
    }
}

export default Services;
